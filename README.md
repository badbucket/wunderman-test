wunderman_test

Instructions:
•	Use Bootstrap 4 to recreate the attached design
•	Bootstrap 4 should be included as a npm package
•	Use SASS to achieve following points:
-	Only include Bootstrap components which are required for the project
-	Overwrite Bootstrap variables whenever possible rather than restyle Bootstrap components
-	Make use of Bootstrap integrated variables and mixins when needed
•	The site should be accessible on all modern devices and browsers (phone, tablet, desktop)
•	The site should be W3C valid
•	The site should make use of HTML5 semantic elements in a meaningful way (HTML5 outline)
•	Choose webpack or gulp to bundle and minify the SASS files
